import random
import logging
from typing import NamedTuple, Optional, List

# This will work if number of tasks in the system is large
# Unfortunately, the chance of hash collision is about 40% for 2**16 tasks
# which is probably reachable in case the system is used by multiple users
# Another thing is representing ID as int - current value should be ok but
# once increased, it might cause a problems in some languages on parsing (e.g.
# JavaScript, see
# https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER)
# TODO: increase the space of possible Task IDs if number of stored tasks start
# to rise
MAX_TASK_ID = 2**32 - 1


class Task(NamedTuple):
    task_id: int
    label: str
    completed: bool
    parent_id: Optional[int] = None

    @classmethod
    def from_db_dict(cls, task_dict: dict):
        parent_id = task_dict.get(b'parent_id')
        if parent_id is not None:
            parent_id = int(parent_id)
        return cls(
            task_id=int(task_dict[b'id']),
            label=task_dict[b'label'].decode('utf-8'),
            completed=task_dict[b'completed'] == b'True',
            parent_id=parent_id)

    @classmethod
    def from_dict(cls, task_dict: dict):
        return cls(
            task_id=task_dict['id'], label=task_dict['label'],
            completed=task_dict['completed'],
            parent_id=task_dict.get('parent_id'))

    @classmethod
    def generate_task_id(self) -> int:
        # TODO: consider using str instead of int
        return random.randint(0, MAX_TASK_ID)

    def to_dict(self) -> dict:
        task_dict = {
            'id': self.task_id,
            'label': self.label,
            'completed': self.completed,
        }
        if self.parent_id is not None:
            task_dict['parent_id'] = self.parent_id

        return task_dict

    def to_db_dict(self):
        db_dict = {
            b'id': self.task_id,
            b'label': self.label.encode('utf-8'),
            b'completed': b'True' if self.completed else b'False',
        }
        if self.parent_id is not None:
            db_dict[b'parent_id'] = self.parent_id

        return db_dict


class TaskUpdate(NamedTuple):
    task_id: int
    label: Optional[str]
    completed: Optional[bool]

    def get_updates(self):
        if self.label is not None:
            yield b'label', self.label.encode('utf-8')
        if self.completed is not None:
            yield b'completed', b'True' if self.completed else b'False'


def generate_task_tree(tasks: List[Task]) -> List[dict]:
    """
    Generate nested task representation from flat list of tasks.
    """
    # Current algorithm is  O(n^2) because Python dict has amortized worst
    # complexity for single set O(n) even though average is O(1). The function
    # can be optimized in the future in case it becomes a bottleneck

    id_to_task_mapping = {}
    for t in tasks:
        task_dict = t.to_dict()
        # add tasks key even if there are not subtasks so the client does not
        # have to check if key exists
        task_dict.setdefault('tasks', [])
        # do not propagate parent ID as the parent is obvious
        task_dict.pop('parent_id', None)
        id_to_task_mapping[t.task_id] = task_dict

    root = []
    for t in tasks:
        task_dict = id_to_task_mapping[t.task_id]

        if t.parent_id is None:
            root.append(task_dict)
            continue

        # do not fail on inconsistent input, i.e. if orphan subtasks are
        # present, instead log it and resolved higher level - some of the
        # assumptions do not hold as this situation should not happen unless
        # somebody manually changed state in the database
        try:
            parent_task_dict = id_to_task_mapping[t.parent_id]
        except KeyError:
            # TODO: setup alert for this log
            logging.error(
                f'Orphan task {t.task_id} found, something is wrong.')
            continue

        parent_task_dict['tasks'].append(task_dict)

    return root
