from typing import Iterator, NamedTuple

from redis import Redis

from avocode.task import Task

NONEXISTENT_ERR_TEMPLATE = 'Task ID {task_id} does not exist.'

update_task_script = """
local task_key = KEYS[1]
local key_exists = redis.call('EXISTS', task_key);
if key_exists == 0 then
    return 0
end

for i = 2,#KEYS do
    redis.call('HSET', task_key, KEYS[i], ARGV[i]);
end
local task = redis.call('HGETALL', task_key)

return task;
"""

# it is unfortunate that task key is defined at two places
# TODO: think of possible refactor
delete_task_script = """
local function get_task_data_key(task_id)
    return 'task.' .. task_id .. '.data'
end

local function get_task_subtask_key(task_id)
    return 'task.' .. task_id .. '.subtask'
end

local function delete_subtask(task_id)
    local task_subtask_key = get_task_subtask_key(task_id)
    local subtask_ids = redis.call('LRANGE', task_subtask_key, 0, -1)
    redis.call('DEL', task_subtask_key)

    for _, subtask_id in pairs(subtask_ids) do
        local subtask_data_key = get_task_data_key(subtask_id)
        redis.call('DEL', subtask_data_key);
        delete_subtask(subtask_id)
    end
end

local task_id = KEYS[1]
local task_data_key = get_task_data_key(task_id)

local key_deleted = redis.call('DEL', task_data_key)
if key_deleted == 0 then
    return 0
end

delete_subtask(task_id)

return 1
"""


class NonExistentTask(Exception):
    pass


class TaskAlreadyExists(Exception):
    pass


class TaskKey(NamedTuple):
    task_id: int
    PREFIX = 'task'

    @property
    def data(self):
        return f'{self.PREFIX}.{self.task_id}.data'

    @property
    def subtask(self):
        return f'{self.PREFIX}.{self.task_id}.subtask'


class TaskDb(Redis):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._update_task = self.register_script(update_task_script)
        self._delete_task = self.register_script(delete_task_script)

    def get_tasks(self) -> Iterator[Task]:
        all_tasks_key = TaskKey('*')
        tasks_ids = [
            _parse_task_id(k.decode()) for k in self.keys(all_tasks_key.data)]
        for task_id in tasks_ids:
            key = TaskKey(task_id)
            task_dict = self.hgetall(key.data)
            # the task might be deleted after keys read
            # TODO: make read atomic
            if not task_dict:
                continue
            yield Task.from_db_dict(task_dict)

    def update_task(self, task_update: dict):
        key = TaskKey(task_update.task_id)
        update_keys, update_values = zip(*task_update.get_updates())
        # pass key value pairs on the same position in the list to prevent off
        # by one bugs
        # TODO: update the script to recursively update the state of subtasks
        # completed -> if parent is completed, subtasks are implicitly
        # completed
        r = self._update_task(
            keys=[key.data, *update_keys], args=[b'', *update_values])
        if r == 0:
            raise NonExistentTask(
                f'Task ID {task_update.task_id} does not exist.')

        task_dict = {r[i]: r[i+1] for i in range(0, len(r), 2)}
        return Task.from_db_dict(task_dict)

    def store_task(self, task: Task):
        key = TaskKey(task.task_id)

        if self.exists(key.data):
            raise TaskAlreadyExists(f'Task ID {task.task_id} already exists.')

        # TODO: the operation below should be done atomically, otherwise it is
        # possible that parent is deleted just between the check and the store
        # of data --> use Lua script
        if task.parent_id is not None:
            parent_key = TaskKey(task.parent_id)
            if not self.exists(parent_key.data):
                raise TaskAlreadyExists(
                    f'Parent task ID {task.parent_id} does not exists.')
            self.lpush(parent_key.subtask, task.task_id)

        self.hmset(key.data, task.to_db_dict())

    def delete_task(self, task_id):
        r = self._delete_task(keys=[task_id])
        if r == 0:
            raise NonExistentTask(f'Task ID {task_id} does not exist.')


def _parse_task_id(task_data_key) -> int:
    _, task_id, _ = task_data_key.split('.')
    return int(task_id)
