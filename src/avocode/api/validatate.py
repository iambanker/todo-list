import json
from json.decoder import JSONDecodeError

from voluptuous import All, Length, Optional, Range, Required, Schema
from voluptuous.error import MultipleInvalid

from avocode.api.error import ApiException
from avocode.task import MAX_TASK_ID, Task, TaskUpdate


def parse_tasks_request(raw_request) -> Task:
    request_dict = _parse_json_request(raw_request, tasks_schema)

    label = request_dict['label']
    parent_id = request_dict.get('parent_id')

    task_id = Task.generate_task_id()
    return Task(
        task_id=task_id, label=label, completed=False, parent_id=parent_id)


def parse_tasks_id_request(raw_request, task_id: int) -> TaskUpdate:
    request_dict = _parse_json_request(raw_request, tasks_id_schema)

    label = request_dict.get('label')
    completed = request_dict.get('completed')

    return TaskUpdate(task_id=task_id, label=label, completed=completed)


def _parse_json_request(raw_request, schema: Schema) -> dict:
    try:
        request_dict = json.loads(raw_request)
        request_dict = schema(request_dict)
    except (JSONDecodeError, MultipleInvalid) as e:
        raise ApiException(400, f'Invalid JSON request: {str(e)}.')

    return request_dict


id_schema = All(int, Range(min=0, max=MAX_TASK_ID))
label_schema = All(str, Length(min=1))
completed_schema = bool

tasks_schema = Schema({
    Required('label'): label_schema,
    Optional('parent_id'): id_schema,
})
tasks_id_schema = All(
    Schema({
        Optional('label'): label_schema,
        Optional('completed'): completed_schema,
    }),
    Length(min=1, max=2),
)
