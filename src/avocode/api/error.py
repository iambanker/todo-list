import logging

from flask import jsonify, make_response
from werkzeug.exceptions import HTTPException


class ApiException(Exception):

    def __init__(self, status_code: int, error_message: str):
        self.status_code = status_code
        self.error_message = error_message


def handle_exception(error):
    # handle custom exception
    if isinstance(error, ApiException):
        err_msg = error.error_message
        status_code = error.status_code
    # and exception raised by Flask
    elif isinstance(error, HTTPException):
        err_msg = error.description
        status_code = error.code
    # if not handled exception, return server error
    else:
        logging.exception(error)
        err_msg = 'Something went terribly wrong. Please try again later.'
        status_code = 500

    error_dict = {'error': err_msg}
    return make_response(jsonify(error_dict), status_code)
