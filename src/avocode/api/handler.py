from flask import Response, jsonify, make_response, request
from flask.views import MethodView

from avocode.api.error import ApiException
from avocode.api.validatate import parse_tasks_id_request, parse_tasks_request
from avocode.db import NonExistentTask, TaskDb
from avocode.task import generate_task_tree


class TasksHandler(MethodView):

    def get(self):
        db = TaskDb()
        tasks = list(db.get_tasks())
        task_tree = generate_task_tree(tasks)
        return _create_response({'tasks': task_tree})

    def post(self):
        task = parse_tasks_request(request.data)

        db = TaskDb()
        # this might fail if generated ID is already stored in DB (hash
        # collision)
        # TODO: document that client should retry on 5xx or even better fix
        # this by retrying ourself
        db.store_task(task)

        return _create_response(task.to_dict(), status_code=201)


class TasksIdHanlder(MethodView):

    def post(self, task_id: int):
        task_update = parse_tasks_id_request(request.data, task_id)

        db = TaskDb()
        try:
            task = db.update_task(task_update)
        except NonExistentTask as e:
            raise ApiException(404, str(e))

        return _create_response(task.to_dict())

    def delete(self, task_id: int):
        db = TaskDb()
        try:
            db.delete_task(task_id)
        except NonExistentTask as e:
            raise ApiException(404, str(e))

        return ('', 204)


def _create_response(response, status_code=200) -> Response:
    """
    :param response: JSON serializable object
    """
    r = jsonify(response)
    if status_code is not None:
        r = make_response(r, status_code)

    return r
