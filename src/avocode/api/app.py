import logging

from flask import Flask

from avocode.api.error import handle_exception
from avocode.api.handler import TasksHandler, TasksIdHanlder


def create_app():
    logging.basicConfig(level=logging.INFO)

    app = Flask('TodoListAPI')

    tasks_hanlder = TasksHandler.as_view('tasks_api')
    tasks_id_handler = TasksIdHanlder.as_view('tasks_id_api')

    app.add_url_rule(
        '/tasks', view_func=tasks_hanlder, methods=['GET', 'POST'])
    app.add_url_rule(
        '/tasks/<int:task_id>', view_func=tasks_id_handler,
        methods=['POST', 'DELETE'])
    app.register_error_handler(Exception, handle_exception)

    return app


app = create_app()
