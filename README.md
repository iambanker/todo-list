# Todo List API

Server application implementing API for management of TODO lists.

## Run

```
docker build -f docker/Dockerfile -t todo-list .
docker run -p 8080:8080 todo-list
```

## Test

```
pytest test/
```

## Architecture

The service is composed of two components:

* Flask service implementing REST API
* Redis instance used for a storage

For ease of use, these two components are bundled together and managed by
[http://supervisord.org/](supervisord). The unfortunate consequence of this
design is that the storage is not persistent across restarts.
