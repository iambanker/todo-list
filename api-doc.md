# API Documentation

## Endpoints:

### `GET /tasks`

Returns a list of tasks.

```
> GET /tasks

< 200 OK
{
  tasks: Task[] = [
    { id: number, label: string, completed: boolean, tasks: Task[] }
  ]
}
```

### `POST /tasks`

Creates a new task. If `parent_id` is specified, the created task is added as
a sub task of task `parent_id`.

```
> POST /tasks
{ label: string } |
{ label: string, parent_id: int }

< 201 Created
{
  task: { id: number, label: string, completed: boolean }
}
```

### `POST /tasks/:id`

Updates the task of the given ID.

```
> POST /tasks/:id
{ label: string } |
{ completed: boolean } |
{ label: string, completed: boolean }

< 200 OK
{
  task: Task = { id: number, label: string, completed: boolean }
}

< 404 Not Found
{ error: string }
```

### `DELETE /tasks/:id`

Deletes the task of the given ID.

```
> DELETE /tasks/:id

< 204 No Content

< 404 Not Found
{ error: string }
```
