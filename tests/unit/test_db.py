from avocode import db


def test_parse_task_id():
    task_id = 42
    task_key = db.TaskKey(task_id)

    assert db._parse_task_id(task_key.data) == task_id
