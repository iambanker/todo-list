from avocode.api.error import ApiException, handle_exception
from werkzeug.exceptions import NotFound


def test_handle_exception(app_ctx):
    api_exception = ApiException(400, 'So request, such bad.')

    with app_ctx:
        response = handle_exception(api_exception)

    assert response.status_code == 400
    assert response.json == {'error': 'So request, such bad.'}

    flask_exception = NotFound('Just cannot find it.')
    with app_ctx:
        response = handle_exception(flask_exception)

    assert response.status_code == 404
    assert response.json == {'error': 'Just cannot find it.'}

    unhandled_exception = Exception('This is internal message.')

    with app_ctx:
        response = handle_exception(unhandled_exception)

    expected_err = 'Something went terribly wrong. Please try again later.'
    assert response.status_code == 500
    assert response.json == {'error': expected_err}
