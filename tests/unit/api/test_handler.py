from avocode.db import NonExistentTask
from avocode.task import Task, TaskUpdate


def test_tasks_handler_get(client, mocker):
    tasks = [
        Task(1, 'very', False),
        Task(10, 'important', True),
        Task(10000, 'to-do', True),
    ]

    get_tasks_mock = mocker.patch('avocode.api.handler.TaskDb.get_tasks')
    get_tasks_mock.return_value = tasks

    resp = client.get('/tasks')

    task_dicts = [t.to_dict() for t in tasks]
    for t in task_dicts:
        t['tasks'] = []

    expected_json = {'tasks': task_dicts}
    _assert_response(resp, expected_json, 200)
    get_tasks_mock.assert_called_once_with()

    # check that the implementation is strict and do not allow trailing
    # slash as it has semantic meaning for other API endpoints (and eventually
    # if GET is implemented for single task ID)
    resp = client.get('/tasks/')
    err_msg = (
        'The requested URL was not found on the server.  If you entered '
        'the URL manually please check your spelling and try again.')
    _assert_response(resp, {'error': err_msg}, 404)


def test_tasks_handler_post(client, mocker):
    task = Task(task_id=22, label='amazing', completed=False)
    store_task_mock = mocker.patch('avocode.api.handler.TaskDb.store_task')
    task_mock = mocker.patch('avocode.api.validatate.Task.generate_task_id')
    task_mock.return_value = task.task_id

    resp = client.post('/tasks', json={'label': task.label})
    _assert_response(resp, task.to_dict(), 201)
    store_task_mock.assert_called_once_with(task)

    resp = client.post('/tasks')
    expected_err = (
        f'Invalid JSON request: Expecting value: line 1 column 1 (char 0).')
    _assert_response(resp, {'error': expected_err}, 400)

    resp = client.post('/tasks', json={})
    expected_err = (
        f"Invalid JSON request: required key not provided @ data['label'].")

    _assert_response(resp, {'error': expected_err}, 400)

    # test subtask can be created
    subtask = Task(
        task_id=23, label='amazing sub task', completed=False,
        parent_id=22)
    store_task_mock = mocker.patch('avocode.api.handler.TaskDb.store_task')
    task_mock = mocker.patch('avocode.api.validatate.Task.generate_task_id')
    task_mock.return_value = subtask.task_id

    request_dict = {
        'label': subtask.label,
        'parent_id': subtask.parent_id,
    }
    resp = client.post('/tasks', json=request_dict)
    _assert_response(resp, subtask.to_dict(), 201)
    store_task_mock.assert_called_once_with(subtask)


def test_tasks_id_handler_post(client, mocker):
    task = Task(task_id=22, label='amazing', completed=False)
    update_task_mock = mocker.patch('avocode.api.handler.TaskDb.update_task')
    update_task_mock.return_value = task

    dict_update = {'label': task.label}

    resp = client.post(f'/tasks/{task.task_id}', json=dict_update)

    _assert_response(resp, task.to_dict(), 200)
    task_update = TaskUpdate(task_id=22, label='amazing', completed=None)
    update_task_mock.assert_called_once_with(task_update)


def test_tasks_id_handler_delete(client, mocker):
    delete_task_mock = mocker.patch('avocode.api.handler.TaskDb.delete_task')
    task_id = '123'

    resp = client.delete(f'/tasks/{task_id}')
    _assert_response(resp, None, 204, 'text/html; charset=utf-8')

    non_existent_task_id = '1234'

    err_msg = 'This message is propagated to the client.'
    delete_task_mock.side_effect = NonExistentTask(err_msg)
    resp = client.delete(f'/tasks/{non_existent_task_id}')

    _assert_response(resp, {'error': err_msg}, 404)
    delete_task_mock.assert_has_calls([
        mocker.call(int(task_id)), mocker.call(int(non_existent_task_id))])


def _assert_response(resp, resp_dict, status_code,
                     content_type='application/json'):
    assert resp.json == resp_dict
    assert resp.status_code == status_code
    assert resp.headers['content-type'] == content_type
