from flask import Flask
from avocode.api.app import app


def test_app():
    assert isinstance(app, Flask)
    assert app.debug is False
