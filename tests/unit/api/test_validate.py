import pytest
from voluptuous.error import MultipleInvalid

from avocode.api.validatate import tasks_id_schema, tasks_schema
from avocode.task import MAX_TASK_ID


def test_tasks_schema():
    tasks_schema({'label': 'nice'})
    tasks_schema({'label': 'ugly', 'parent_id': MAX_TASK_ID})

    with pytest.raises(MultipleInvalid):
        tasks_schema({})

    with pytest.raises(MultipleInvalid):
        tasks_schema({'label': 'pretty', 'parent_id': MAX_TASK_ID + 1})


def test_tasks_id_schema():
    tasks_id_schema({'label': 'nice'})
    tasks_id_schema({'completed': False})
    tasks_id_schema({'label': 'ugly', 'completed': True})

    with pytest.raises(MultipleInvalid):
        tasks_id_schema({})

    with pytest.raises(MultipleInvalid):
        tasks_id_schema({'completed': False, 'invalid': 'key'})
