import pytest
from flask import Flask

from avocode.api.app import app


@pytest.fixture
def client():
    client = app.test_client()

    yield client


@pytest.fixture
def app_ctx():
    app = Flask(__name__)
    yield app.app_context()
