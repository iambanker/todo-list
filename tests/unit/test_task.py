from avocode.task import Task, generate_task_tree


def test_task():
    task = Task(1, 'label', False)

    dict_de_serialized_task = Task.from_dict(task.to_dict())
    db_de_serialized_task = Task.from_db_dict(task.to_db_dict())
    assert task == dict_de_serialized_task
    assert task == db_de_serialized_task

    assert 0 <= Task.generate_task_id() < 2**32


def test_generate_task_tree():
    tasks = [
        Task(4, 'label', True, parent_id=2),
        Task(2, 'label', False, parent_id=1),
        Task(1, 'label', True),
        Task(3, 'label', False, parent_id=2),
        Task(42, 'label', True),
    ]

    task_tree = generate_task_tree(tasks)
    expected_task_tree = [
        {
            "id": 1,
            "label": "label",
            "completed": True,
            "tasks": [
                {
                    "id": 2,
                    "label": "label",
                    "completed": False,
                    "tasks": [
                        {
                            "id": 4,
                            "label": "label",
                            "completed": True,
                            "tasks": []
                        },
                        {
                            "id": 3,
                            "label": "label",
                            "completed": False,
                            "tasks": []
                        }
                    ]
                }
            ]
        },
        {
            "id": 42,
            "label": "label",
            "completed": True,
            "tasks": []
        }
    ]

    assert task_tree == expected_task_tree

    orphan_tasks = [
        Task(4, 'label', False, parent_id=2),
        Task(3, 'label', False, parent_id=1),
        Task(42, 'label', False),
    ]
    # check the method does not fail if orphans exist
    orphan_task_tree = generate_task_tree(orphan_tasks)

    expected_orphan_task_tree = [
        {
            "id": 42,
            "label": "label",
            "completed": False,
            "tasks": []
        }
    ]
    assert orphan_task_tree == expected_orphan_task_tree

    assert generate_task_tree([]) == []
