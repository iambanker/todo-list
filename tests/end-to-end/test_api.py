import requests

from avocode.task import Task

API_URL = 'http://127.0.0.1:8080'
DELETE_LABELS = {'__test__ delete one', '__test__ delete two'}
UPDATE_LABELS = {'__test__ update one', '__test__ update two'}
CREATE_LABELS = {'__test__ create one', '__test__ create two'}


def test_create_update_delete_get_workflow():
    tasks = []
    for label in (*CREATE_LABELS, *UPDATE_LABELS, *DELETE_LABELS):
        r = requests.post(f'{API_URL}/tasks', json={'label': str(label)})
        r.raise_for_status()

        tasks.append(r.json())

    delete_task_ids = (t['id'] for t in tasks if t['label'] in DELETE_LABELS)
    for delete_task_id in delete_task_ids:
        r = requests.delete(f'{API_URL}/tasks/{delete_task_id}')
        r.raise_for_status()

    update_task_ids = (t['id'] for t in tasks if t['label'] in UPDATE_LABELS)
    for update_task_id in update_task_ids:
        r = requests.post(
            f'{API_URL}/tasks/{update_task_id}', json={'completed': True})
        r.raise_for_status()

    r = requests.get(f'{API_URL}/tasks')
    r.raise_for_status()

    stored_tasks = r.json()['tasks']

    expected_labels = set((*CREATE_LABELS, *UPDATE_LABELS))
    labels = set(t['label'] for t in stored_tasks)

    assert labels.issuperset(expected_labels)
    assert all(l not in labels for l in DELETE_LABELS)
    assert all(
        t['completed'] for t in stored_tasks if t['label'] in UPDATE_LABELS)

    # clean up
    for task in stored_tasks:
        r = requests.delete(f"{API_URL}/tasks/{task['id']}")


def test_subtask_workflow():
    r = requests.post(f'{API_URL}/tasks', json={'label': 'parent'})
    r.raise_for_status()
    parent_task = Task.from_dict(r.json())

    subtask_dict = {'label': 'subtask', 'parent_id': parent_task.task_id}
    r = requests.post(f'{API_URL}/tasks', json=subtask_dict)
    r.raise_for_status()
    subtask = Task.from_dict(r.json())

    subsubtask_dict = {'label': 'subsubtask', 'parent_id': subtask.task_id}
    r = requests.post(f'{API_URL}/tasks', json=subsubtask_dict)
    r.raise_for_status()
    subsubtask = Task.from_dict(r.json())

    r = requests.get(f'{API_URL}/tasks')
    r.raise_for_status()
    stored_tasks = r.json()['tasks']
    (stored_parent_task,) = (
        t for t in stored_tasks if t['label'] == parent_task.label)

    # parent ID should not be part of the response
    subtask_dict = subtask.to_dict()
    del subtask_dict['parent_id']
    subsubtask_dict = subsubtask.to_dict()
    del subsubtask_dict['parent_id']

    expected_task_dict = {
        **parent_task.to_dict(),
        'tasks': [{
            **subtask_dict,
            'tasks': [{
                **subsubtask_dict,
                'tasks': []}]
        }]
    }
    assert stored_parent_task == expected_task_dict

    # clean up - it is enough to just delete parent_task as subtasks are
    # removed transitively
    r = requests.delete(f"{API_URL}/tasks/{parent_task.task_id}")

    r = requests.get(f'{API_URL}/tasks')
    r.raise_for_status()
